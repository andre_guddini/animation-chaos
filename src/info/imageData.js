import firstImage from '../assets/1.gif'
import secondImage from '../assets/2.gif'
import thirdImage from '../assets/3.gif'
import fourthImage from '../assets/4.gif'
import sixthImage from '../assets/7.gif'
import seventhImage from '../assets/8.gif'
import eighthImage from '../assets/9.gif'
import ninthImage from '../assets/10.gif'
import eleventhImage from '../assets/12.gif'
import twelvethImage from '../assets/13.gif'
import thirtheenthImage from '../assets/14.gif'
import fourteenthImage from '../assets/15.gif'

const imagesArray = [
    firstImage,
    secondImage,
    thirdImage,
    fourthImage,
    sixthImage,
    seventhImage,
    eighthImage,
    ninthImage,
    eleventhImage,
    twelvethImage,
    thirtheenthImage,
    fourteenthImage,
  ];
  
  export default imagesArray;