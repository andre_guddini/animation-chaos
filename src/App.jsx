import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Welcome from './pages/Welcome';
import ChaosPage from './pages/ChaosPage';
import ChaosElement from './pages/ChaosElement';

const router = createBrowserRouter([
  { path: '/', element: <Welcome /> },
  { path: '/chaos', element: <ChaosPage /> },
  {path: '/chaos/:id', element: <ChaosElement />}
]);

function App() {
  return  <RouterProvider router={router} />;
  }

export default App;