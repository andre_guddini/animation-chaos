import { motion, useScroll, useTransform } from 'framer-motion'
import seventhImage from '../assets/6.gif'
import { Link } from 'react-router-dom'
import { useState } from 'react';

function Welcome() {
    const [isHovered, setIsHovered] = useState(false);
    const { scrollY } = useScroll();
    const yImage = useTransform(scrollY, [0, 200, 300, 400, 500], [0, 100, 150, 100, 100]);
    const imageZoom = useTransform(scrollY, [0, 100, 200, 300, 400, 500, 700, 900, 1000], [1.1, 1.3, 1.5, 1.6, 1.7, 1.8, 1.7, 1.5, 1.3])
    return (

        <motion.div id='welcome-header'>
            <motion.img src={seventhImage} style={{ y: yImage, scale: imageZoom }} className='starting-image'></motion.img>
            <Link to="./chaos">
                <motion.div className="center-click-zone" onMouseEnter={() => setIsHovered(true)}
                    onMouseLeave={() => setIsHovered(false)}
                    initial={{ opacity: 0 }} animate={{
                        opacity: isHovered ? 1 : 0, rotate: [0,180, 360, 0,180,360], color:['white','blue','yellow','orange','white']
                    }} transition={{
                        duration: 0.5,
                        ease: "easeInOut",
                        times: [0, 0.2, 0.5, 0.8, 1],
                        repeat: Infinity,
                        repeatDelay: 0.5
                    }}
                >{'Ready?'}</motion.div>
            </Link>
        </motion.div>

    )
}

export default Welcome;