import { motion } from 'framer-motion'
import { useParams } from 'react-router-dom'
import imagesArray from '../info/imageData';
import { Link } from 'react-router-dom';
function ChaosElement() {
    const { id } = useParams();
    const imageUrl = imagesArray[id];
   


    return <motion.div
        className='chaos-div single-page-el'
        style={{
            width: '100%',
            height: '120vh',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url(${imageUrl})`,
        }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
    >
        <motion.div  className='center-click-zone single-element-center'><Link to='../chaos' className='single-element-link'>Get Back</Link></motion.div>
        
    </motion.div>
}

export default ChaosElement