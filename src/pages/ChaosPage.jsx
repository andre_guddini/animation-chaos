import { motion} from 'framer-motion'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import imagesArray from '../info/imageData';



function ChaosPage() {
  const [backgroundImageIndex, setBackgroundImageIndex] = useState(0);
  const [leftAnimation, setLeftAnimation] = useState(0);  
  const [rightAnimation, setRightAnimation] = useState(0);
  const [topAnimation,setTopAnimation] = useState(0) ;
  const [bottomAnimation, setBottomAnimation] = useState(0);
  
  useEffect(() => {
    // Function to update the background image index
    const updateBackgroundImage = () => {
      setBackgroundImageIndex((prevIndex) => (prevIndex + 1) % imagesArray.length);
      setLeftAnimation((Math.random()*50).toFixed(0));
      setRightAnimation((Math.random()*50).toFixed(0));
      setTopAnimation((Math.random()*50).toFixed(0));
      setBottomAnimation((Math.random()*50).toFixed(0));
    };
    
    // Set an interval to change the background image every second
    const intervalId = setInterval(updateBackgroundImage, 1000);
    // Clean up the interval when the component unmounts
    return () => {
      clearInterval(intervalId);
    };
  }, []);
  

  return (
    <Link to={`${backgroundImageIndex}`}>
    <motion.div
      className='chaos-div'
      style={{
        width: '100%',
        height: '100vh',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundImage: `url(${imagesArray[backgroundImageIndex]})`,
      }}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
        {imagesArray && <>
      <Link to={`${backgroundImageIndex+1 % imagesArray.length}`}><motion.img  src={imagesArray[(backgroundImageIndex + 1) % imagesArray.length]} className='chaos-image top-left' 
      initial={{ top: 0, left: 0 }}  animate={{ top: `${topAnimation}%`, left: `${leftAnimation}%` }} 
      transition={{ duration: 1 }}></motion.img></Link>
      <Link to ={`${backgroundImageIndex+2 % imagesArray.length}`}><motion.img  src={imagesArray[(backgroundImageIndex + 2) % imagesArray.length]} className='chaos-image top-right' 
      initial={{ top: 0, right: 0 }}  animate={{ top: `${topAnimation}%`, right: `${rightAnimation}%` }} 
      transition={{ duration: 1 }}></motion.img></Link>
      <Link to ={`${backgroundImageIndex+3 % imagesArray.length}`}><motion.img  src={imagesArray[(backgroundImageIndex + 3) % imagesArray.length]} className='chaos-image bottom-left' initial={{ bottom: 0, left: 0 }}
       animate={{ bottom: `${bottomAnimation}%`, left: `${leftAnimation}%` }} 
       transition={{ duration: 1 }}></motion.img></Link>
     <Link to ={`${backgroundImageIndex+4 % imagesArray.length}`}> <motion.img  src={imagesArray[(backgroundImageIndex + 4) % imagesArray.length]} className='chaos-image bottom-right' initial={{ bottom: 0, right: 0 }}
       animate={{ bottom: `${bottomAnimation}%`, right: `${rightAnimation}%` }} 
       transition={{ duration: 1 }}></motion.img></Link>
       </>
   }
       </motion.div>
       </Link>
    
  )
}

export default ChaosPage